# Airflow 설치 가이드

## 준비사항
- chart 준비
    - airflow 공식 설치 차트 8.6.1 버전에서 공통사용 변수들 수정된 버전
    - ./pipeline-helm/chart/airflow
- yaml 준비
    - redis, mysql, pvc 생성 yaml
    - ./pipeline-helm/chart/airflow/aiip-install-dependency

### chart value 수정
- values.yaml
    - pipeline customized airflow 이미지 이름, 태그 입력
      ```
      image:
        repository: [릴리즈노트에 기재되어 있는 image 명]
        tag: [릴리즈노트에 기재되어 있는 tag]
      ```
    - pipeine service db connection 변경
      ```
      connections:
        id: "aiip_db"
        host: [aiip-pipeline db 주소]
        login: [aiip-pipeline db 접속 아이디]
        password: [aiip-pipeline db 접속 비밀번호]
      ```
- yaml 수정
    - core-airflow-pvc-sub.yaml
        - nfs 주소 변경
      ````
      16  | volumeHandle: [fs-a43ef4c4]:/airflow/dags
      47  | volumeHandle: [fs-a43ef4c4]:/airflow/libs
      78  | volumeHandle: [fs-a43ef4c4]:/airflow/logs
      109 | volumeHandle: [fs-a43ef4c4]:/airflow/python_files
      ````

### Install
1. main pvc 생성
  ````
  kubectl apply -f core-airflow-pvc-main.yaml
  ````
2. airflow 사용 디렉토리 생성 (pipeline 설치시 디렉토리 생성되어있으면 생략가능)
   ```
   kubectl apply -f core-airflow-pvc-mkdir-job.yaml
   ```
    [*중요*] *생성된 디렉토리는 770 이상의 권한이 필요함*
2. 디렉토리별 pvc 생성
````
kubectl apply -f core-airflow-pvc-sub.yaml
````
4. mysql pod 생성
````
kubectl apply -f core-airflow-mysql.yaml
````
5. redis pod 생성
````
kubectl apply -f core-airflow-redis.yaml
````
6. airflow chart 설치
````
(./pipeline-helm/chart/airflow/ 로 이동 후)
helm install -n aiip-common airflow airflow
````
7. istio 설정
    1. yaml 수정
        1. core-airflow-virtualservice.yaml 에서 gateways 이름과 hosts 이름을 기존 aiip-common 에 설정되어있는 gateway 로 변경한다.
           ````
             gateways:
               - aiip-istio-gateway.istio-system.svc.cluster.local
                 hosts:
               - aiip-dev.skcc.com
               - 0d0e97d3-istiosystem-istio-2af2-883717537.ap-northeast-2.elb.amazonaws.com
           ````
    2. yaml 설치
        ````
           kubectl apply -f core-airflow-virtualservice.yaml
        ````

8. 설치확인
    1. url 접속
        1. http://{aiip service url}/airflow/home
        2. 로그인 id (admin) : airflow_admin / !Skeap1235


### 기본 설치 외 추가 변경사항
- 네임스페이스 변경 필요시
    1. helm chart - value.yaml
        ````
        1692 | host: core-airflow-mysql.[namespace].svc.cluster.local
        2152 | host: core-airflow-redis.[namespace].svc.cluster.local
       ````
    2. core-*.yaml 들의 namespace 변경
    3. install 명령어 변경
       ````
       (./pipeline-helm/chart/airflow/k8s/init/helm/charts 로 이동 후)
       helm install -n [namespace] airflow airflow
       ````
    4. istio 설정 변경
        ````
          - match: 
            - uri: 
                prefix: /[base_url]
            - uri: 
                prefix: /[base_url]/ 
            rewrite: 
              uri: /[base_url] 
            route: 
            - destination: 
                host: airflow-web.[namespace].svc.cluster.local 
                port: 
                  number: 8080 
        ````

- base_url 변경 필요시
    1. helm chart - value.yaml 변경
        ````
        65 | AIRFLOW__WEBSERVER__BASE_URL: "http://my_host/[base_url]"
        ````
    2. istio 설정 변경 (core-airflow-virtualservice.yaml 에서 수정)
        ````
          - match: 
            - uri: 
                prefix: /[base_url]
            - uri: 
                prefix: /[base_url]/ 
            rewrite: 
              uri: /[base_url] 
            route: 
            - destination: 
                host: airflow-web.aiip-common.svc.cluster.local 
                port: 
                  number: 8080 
        ````
    3. pipeline-interface-server configmap 변경
        ````
        AIRFLOW_SERVER_URL: http://airflow-web.aiip-common.svc.cluster.local:8080/[base_url]/api/v1/dags
        ````
## 차트 관리 
  헬름차트 백업 관리 : 자원 변경시 https://gitlab.com/skdt/aiip/aiipint/stage/helm/서비스명  경로로 자동 push됩니다.(Settings->Mirroring)
