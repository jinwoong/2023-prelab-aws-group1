from airflow.config_templates.default_celery import DEFAULT_CELERY_CONFIG
from kombu import Exchange, Queue

CELERY_TASK_QUEUES = [
    Queue('task1', Exchange('task1', type='direct'), routing_key='task1', queue_arguments={'x-max-priority': 8}),
    Queue('task2', Exchange('task2', type='direct'), routing_key='task2', queue_arguments={'x-max-priority': 6}),
]

CELERY_CONFIG = {
    **DEFAULT_CELERY_CONFIG,
    'database_engine_options': {'pool_size': 5, 'pool_recycle': 1800, 'pool_pre_ping': True}
}
