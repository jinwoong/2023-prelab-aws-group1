"""
dag wirte - dag parsing - dag execute 의 성능측정을 위해 수행되는 DAG
"""

from airflow.models import DAG, DagModel, DagTag, DagRun, ImportError, Variable
from airflow.operators.python import PythonOperator
from airflow import settings
from datetime import timedelta, datetime
from pytz import timezone
import os
import os.path, time
import socket
import logging
import pendulum
import itertools
import pandas as pd
from tabulate import tabulate

pd.options.display.float_format = '{:.2f}'.format

DEFAULT_SCHEDULE = Variable.get("airflow-performance-check-schedule", "@hourly")
DEFAULT_PERFORMANCE_CHECK_INTERVAL = Variable.get("airflow-performance-check-interval", "H")

DAG_ID = os.path.basename(__file__).replace(".pyc", "").replace(".py", "")
START_DATE = pendulum.today('Asia/Seoul').add(days=-1)
# How often to Run. @daily - Once a day at Midnight
SCHEDULE_INTERVAL = DEFAULT_SCHEDULE
# Who is listed as the owner of this DAG in the Airflow Web Server
DAG_OWNER_NAME = "airflow"
DAG_TAGS = ['maintenance', 'airflow-maintenance']
# accutuning 의 dag는 file write -> execute가 연속적으로 이루어지지 않아, 성능 측정 불가능.
IGNORE_TAGS = ['maintenance', 'accutuning']
ALERT_EMAIL_ADDRESSES = "airflow@aa.aa"

seoul_timezone = pendulum.timezone('Asia/Seoul')


default_args = {
    'owner': DAG_OWNER_NAME,
    'email': ALERT_EMAIL_ADDRESSES,
    'email_on_failure': False,
    'email_on_retry': False,
    'start_date': START_DATE,
    'retries': 1,
    'retry_delay': timedelta(minutes=10)
}

dag = DAG(
    DAG_ID,
    default_args=default_args,
    schedule_interval=SCHEDULE_INTERVAL,
    start_date=START_DATE,
    tags=DAG_TAGS,
)

def checkfile():
    session = settings.Session()
    # tag 검사, active 검사.
    dagModels = session.query(DagModel).filter(~DagModel.tags.any(DagTag.name.in_(IGNORE_TAGS))).filter(DagModel.is_active)

    columns = ['dag_id', 'write_time', 'execute_time', 'time_delta']

    rowData = []
    notExecutionDags = []

    for dagModel in dagModels:
        dag = DAG(dagModel.dag_id)
        if os.path.exists(dagModel.fileloc):
            mtime = datetime.strptime(time.ctime(os.path.getmtime(dagModel.fileloc)), "%a %b %d %H:%M:%S %Y").replace(tzinfo=None)

            #todo dag_runs 에 두개 이상있으면 dag_id 재활용 case 제외
            dagRuns = session.query(DagRun).filter(DagRun.dag_id==dag.dag_id)

            #print(f"dag run count: {dagRuns.count()}")

            if (dagRuns.count()==1 and dag.get_latest_execution_date()):
                #timezone convert - AIRFLOW 설정은 Asia/Seoul 이지만, session query 해서 넘어온값이 UTC로 넘어옴.
                #tzinfo 값이 있을떄, dataframe으로 만들면서 오류가 난다. tzinfo가 있는 column, 없는 column 같이 있을때 오류가 나는것같음.
                lastExecutionDate = seoul_timezone.convert(dag.get_latest_execution_date()).replace(tzinfo=None)
                rowData.append([dag.dag_id, mtime, lastExecutionDate, round((lastExecutionDate - mtime).total_seconds(), 2)])
            else:
                notExecutionDags.append([dag.dag_id, mtime, '', ''])

    df = pd.DataFrame(rowData, columns=columns)

    summary = df[['write_time', 'execute_time', 'time_delta']]
    summary = summary.resample(DEFAULT_PERFORMANCE_CHECK_INTERVAL, on='write_time').mean().dropna()

    print(f"\n === Performance report ===\n{tabulate(summary[['time_delta']], headers=['Hourly', 'Execute delay [s]'], tablefmt='psql')}")

    print(f"\n === raw data ===\n {tabulate(df, headers='keys', tablefmt='psql')}")


checkfile_dags = PythonOperator(
    task_id = 'checkfile',
    pool = "maintenance_pool",
    python_callable=checkfile,
    dag=dag)
